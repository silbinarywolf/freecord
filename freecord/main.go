package main

import (
	"log"
	"os"

	"github.com/silbinarywolf/freecord/freecord/internal/bootstrap"
	"github.com/silbinarywolf/freecord/freecord/internal/configuration"
)

func main() {
	config, err := configuration.LoadDefaultConfig()
	if err != nil {
		log.Print(err, "\n")
		os.Exit(2)
	}
	if err := bootstrap.Start(config); err != nil {
		log.Print(err, "\n")
		os.Exit(1)
	}
}
