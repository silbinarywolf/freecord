package db

import (
	"context"
	"database/sql"
	"strings"
	"time"

	"github.com/pkg/errors"
)

const (
	// It might be more appropriate for these constants to be configurable instead,
	// but we can always decide to do that later. For now, this is probably good enough.
	maxDBRetries         = 5
	timeBetweenDBRetries = 2 * time.Second
)

type Settings struct {
	Host     string
	Port     int
	User     string
	Password string
	// Opted to not implement for time reasons. We just use Postgres's default database.
	// DatabaseName string
}

func Connect(databaseHost string) (*sql.DB, error) {
	// NOTE(jae): 2021-04-26
	// The following is supported by the Postgres SQL driver:
	// - sql.Open("postgres", "postgres://bob:secret@1.2.3.4:5432/mydb?sslmode=verify-full")
	driverAndUrlString := strings.SplitN(databaseHost, ":", 2)
	if len(driverAndUrlString) == 1 {
		return nil, errors.New(`expected dsn format for database driver, ie. "postgres://bob:secret@1.2.3.4:5432/mydb?sslmode=verify-full"`)
	}
	db, err := sql.Open(driverAndUrlString[0], databaseHost)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	// ping the DB host.
	// we do this to account for server setups where the database host may take a while to boot-up
	if err := db.PingContext(context.Background()); err != nil {
		db.Close()
		return nil, errors.Wrap(err, "timed out trying to ping database after opening a connection")
	}

	return db, nil
}
