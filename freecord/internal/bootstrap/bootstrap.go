package bootstrap

import (
	_ "github.com/lib/pq"

	"github.com/pkg/errors"
	"github.com/silbinarywolf/freecord/freecord/internal/configuration"
	"github.com/silbinarywolf/freecord/freecord/internal/db"
)

func MustStart(config *configuration.Config) {
	if err := Start(config); err != nil {
		panic(err)
	}
}

func Start(config *configuration.Config) error {
	db, err := db.Connect(config.DatabaseHost)
	if err != nil {
		return errors.Wrap(err, "error connecting to database")
	}
	_, err = db.Query("SELECT * FROM jerry")
	panic(err)
	return nil
}
