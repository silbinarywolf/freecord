package configuration

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"strings"

	"github.com/pkg/errors"
)

type Config struct {
	DatabaseHost string
}

func LoadDefaultConfig() (*Config, error) {
	filename := "config.json"
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, errors.Wrapf(err, "unable to load %s", filename)
	}
	config := new(Config)
	d := json.NewDecoder(bytes.NewReader(b))
	d.DisallowUnknownFields()
	if err := d.Decode(config); err != nil {
		return nil, errors.Wrapf(err, "unable to load %s", filename)
	}
	// Set defaults
	// [TODO]
	if err := validateConfig(*config); err != nil {
		return nil, err
	}
	return config, nil
}

type configErrorList struct {
	errors []error
}

var _ error = configErrorList{}

func (errList configErrorList) Error() string {
	var s string = "configuration error(s):"
	for _, err := range errList.errors {
		s += "\n- " + err.Error()
	}
	return s
}

func validateConfig(config Config) error {
	var errorList configErrorList
	if strings.TrimSpace(config.DatabaseHost) == "" {
		errorList.errors = append(errorList.errors, errors.New("DatabaseHost cannot be empty"))
	}
	if len(errorList.errors) > 0 {
		return errorList
	}
	return nil
}
