module github.com/silbinarywolf/freecord

go 1.16

require (
	github.com/lib/pq v1.10.1
	github.com/pkg/errors v0.9.1
)
